import java.util.Scanner;
public class Shop {
public static void main (String[]args){
	Apple[] ap = new Apple[4];
	Scanner dg = new Scanner(System.in);
	int j = 1;
	for (int i = 0; i < ap.length; i++){
			System.out.println("\nSetting values of apple #" + j +"...\n");
			System.out.println("Enter a colour");
			String colour = dg.next(); 
			System.out.println("Enter the size in centimeters. Decimals allowed.");
			double size = dg.nextDouble();
			System.out.println("Enter the longevity of the apple in days.");
			int longevity = dg.nextInt();
			ap[i] = new Apple(colour, size, longevity);

		System.out.println("\nValues of apple #" + j +"...\n");
		System.out.println(ap[i].getColour());
		System.out.println(ap[i].getSize());
		System.out.println(ap[i].getLongevity());
		j++;
		
		}
		System.out.println("\nNow time to update last apple.\n");
			System.out.println("Enter a colour");
			String colour = dg.next(); 
			System.out.println("Enter the size in centimeters. Decimals allowed.");
			double size = dg.nextDouble();
			ap[3].setSize(size);
			System.out.println("Enter the longevity of the apple in days.");
			int longevity = dg.nextInt();
			ap[3].setLongevity(longevity);
			ap[3] = new Apple(colour, size, longevity);
			
	System.out.println("The last product has these following fields: \n");
	ap[3].lastProduct();
	}
}