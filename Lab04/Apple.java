public class Apple {
	public String colour;
	public double size;
	public int longevity;
	
	public Apple (String c, double s, int l){
		this.colour = c;
		this.size = s;
		this.longevity = l;
	}
		
	public String getColour(){
		return this.colour;
		}
		
	public void setSize(double s){
		this.size = s;
		}
		
	public double getSize(){
		return this.size;
		}
		
	public void setLongevity(int l){
		this.longevity = l;
		}
		
	public int getLongevity(){
		return this.longevity;
		}
		
	public void lastProduct(){
		System.out.println("The colour is " + colour + ", the size of the apple is " + size + " centimeters, and the longevity is " + longevity + " days.");
		
	}
}